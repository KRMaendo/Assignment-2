### A Pluto.jl notebook ###
# v0.19.0

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ e43dfac9-4415-47da-9efa-9bf41d23fa46
using PlutoUI

# ╔═╡ 9199ffd8-0b3f-45b3-8392-8480e05e312e
md"
# Prepare for user input
"

# ╔═╡ 16d2cef0-1dd1-40bd-9970-840e7cc47def
md"
## Create office structure
"

# ╔═╡ bbfa0e76-f88a-4c7b-a657-e647ea08454c
mutable struct Office
	parcels::Int64
	position::Int64
	collector::Bool
end

# ╔═╡ 76289f98-8744-4604-bab0-97667db69ee8
md"
## Collect User Input
"

# ╔═╡ ff251553-055b-45a9-8c51-1097193fd78c
begin
	md"""
	Please configure the the problem by inputing the ranges below: \
	Number of storeys:\
	$(@bind num_of_storeys html"<input type=range min=1>")
	"""
end

# ╔═╡ f43d9737-d84c-4e6b-a6e8-c28508f19906
md"""
$num_of_storeys
"""

# ╔═╡ 31d07c38-67cc-485a-89d0-ed5245b68c6e
md"""
Number of offices\
$(@bind num_of_offices html"<input type=range min=1>")
"""

# ╔═╡ 0c2c0c8b-e3e6-4de5-9ed6-4b0a23866e60
md"""
$num_of_offices
"""

# ╔═╡ 53208084-f453-41b6-8454-c44d59eee2fb
begin
	md"""
	Number of parcels\
	$(@bind num_of_parcels html"<input type=range min=1>")
	"""
end

# ╔═╡ bd840ecd-dcc4-4266-9b86-035a1aba7f7f
md"""
$num_of_parcels
"""

# ╔═╡ 3ca2a099-cd62-4dcb-bf0c-9f5024be67d1
md"
## Define and generate problem
"

# ╔═╡ 4663f817-2c70-4809-b121-3fae45c0e690
md"
### Define problem
"

# ╔═╡ 5fa4eea8-1d3a-4840-aed3-d69d3a7ff7c1
function createbuilding(x, y, z)
	building = fill(Office(z, 0, false), (x, y))
	
	count = 1
	for i in 1:x
		for j in 1:y
			building[i, j] = Office(z, count, false)
			count += 1
		end
	end
	return building
end

# ╔═╡ 5cdbaf54-db81-4e65-99b9-602fbc854004
md"
### Generating problem
"

# ╔═╡ 09aab4c7-d334-445b-8c22-89cd0bafbadd
pb = createbuilding(num_of_storeys, num_of_offices, num_of_parcels)

# ╔═╡ 4d7466d0-be0e-4be0-86a4-c497d9c3efca
room_numbers = length(pb)

# ╔═╡ 30757e4f-3409-46a7-916c-affd7381a4d7
md"
### Collect the position for user data
"

# ╔═╡ 3b5eff55-91bc-4e2a-b3f6-e3712639e3ad
begin
	md"""
	Select a postion from 1 to $room_numbers (this matches with the number of rooms available)
	"""
end

# ╔═╡ 4246d386-c09a-43db-8136-f41ae814c9d1
begin
	md"""
	$(@bind position html"<input type=number min=1>")
	"""
end

# ╔═╡ 466944b8-4292-4d2b-b28a-c33e43fb2661
if position > room_numbers
		md"""
		The room number that you have selected is greater than the rooms available
		"""
else
	md"""
	The solving will now begin...
	"""
end

# ╔═╡ 25f4bbb6-1421-48c9-873b-6e66d8ea7ac2
md"
## Defining goal states
"

# ╔═╡ 3db063d0-bc59-45eb-85e3-34f21bbfb47d
function creategoalstate(inputcount, x, y)
	state = fill(Office(0, 1, true), (x, y))
	count = 1
	for i in 1:x
		for j in 1:y
			state[i, j] = count == inputcount ? Office(0, count, true) : Office(0, count, false)
			count += 1
		end
	end
	return state
end

# ╔═╡ d70d8486-3416-4ea2-bb5a-ab07a4052e7c
function create_goal_states(x, y)
	states = []
	count = 1
	for i in 1:length(pb)
		push!(states, creategoalstate(count, x, y))
		count += 1
	end
	return states
end

# ╔═╡ 9bac7bc0-d697-4533-a7ff-1a46ca3b0ccf
md"
### Generating the goal states
"

# ╔═╡ 16ff7843-63c0-42aa-8344-f1695738a287
goalstates = create_goal_states(num_of_storeys, num_of_offices)

# ╔═╡ be180abd-a629-4097-bd0c-cae9fd510ee8
md"
## Defining initial state
"

# ╔═╡ db322b58-f00b-40fd-9f79-0264e080229d
function createinitialstate(x, y, z)
	building = fill(Office(0, 0, false), (x, y))

	count = 1
	for i in 1:x
		for j in 1:y
			building[i, j] = count == position ?  Office(z, count, true) : Office(z, count, false)
			count += 1
		end
	end
	return building
end

# ╔═╡ 93355dd6-56f5-4012-b9d4-54880c969d78
md"
## Generating intial state
"

# ╔═╡ ffee1e39-0b58-4b72-9488-bbb87227724a
initialstate = createinitialstate(num_of_storeys, num_of_offices, num_of_parcels)

# ╔═╡ 9cd8df79-bf0c-4be9-a9f5-699155f7aa59
md"
### Defining actions
"

# ╔═╡ 5a8d8d44-2104-4eb9-bb87-e7a42d27062f
function collect(office)
	office.parcels -= 1
	return office
end

# ╔═╡ a3bc99e6-4330-41bf-b679-2f7562fd1108
function move_up(office)
	return office.position - num_of_offices
end

# ╔═╡ 2887c30d-f356-4c61-bf93-b4a8bb7a1533
function move_down(office)
	return office.position + num_of_offices
end

# ╔═╡ 5457f1ba-4d18-4765-9925-7b089a77be15
function move_left(office)
	return office.position - 1
end

# ╔═╡ 39b56787-03dd-4ade-9fd1-40c82b76a40f
function move_right(office)
	return office.position + 1
end

# ╔═╡ 3de71206-fc15-4cd7-bd01-f9d260633182
md"
## Generating search algorithm
"

# ╔═╡ dfed749d-1c7c-4f28-8a31-071abde42188
md"
## Generate utility functions for search algorithm
"

# ╔═╡ a1724f33-54d0-4b80-b50f-56370cb76132


# ╔═╡ fecacb61-edce-4bf5-a517-e5281a7f567a
function calculatevalue(state, x, y)
	total = 0
	for i in 1:x
		for j in 1:y
			total += state[i, j].parcels * 3
		end
	end
	return total
end

# ╔═╡ 34792a3d-c56c-4aeb-8d76-3dbcf26b87b6
function decide_next_state(possible_states, storeys, offices)
	hurestics = []
	for i in 1:length(possible_states)
		push!(hurestics, [i, calculatevalue(possible_states[i], storeys, offices)])
	end
	
	return current_state
end

# ╔═╡ a46f5b8e-407c-47f7-8c6e-f2742307ee82
huresticvalue = calculatevalue(initialstate, num_of_storeys, num_of_offices)

# ╔═╡ 6b673a17-8d43-4cef-a102-8b9ed15e538e
function generaterightwall(officetotal, x)
	 right = []
	start = 0
	count = 1
	for i in 1:officetotal
		if count == start + x
			push!(right, count)
			start = count
		end
		count += 1
	end
	return right
end

# ╔═╡ 82a85a4c-776e-41cd-a64f-51cf96108e15
rightwall = generaterightwall(length(initialstate), num_of_offices)

# ╔═╡ 40abb265-2f2e-4799-a5dc-a7dfc3008b0e
function generateleftwall(officetotal, x)
	left = []
	start = 1
	count = 1
	for i in 1:officetotal
		if count == start + x || count == 1
			push!(left, count)
			start = count
		end
		count += 1
	end
	return left
end

# ╔═╡ 7980c36d-ffd7-4c17-986a-ba9d3e706fe5
leftwall = generateleftwall(length(initialstate), num_of_offices)

# ╔═╡ 1835eff7-6916-45e5-a5be-cebce87f6cf2
function generatebottomfloor(officetotal, x)
 bottom = []
	start = officetotal - x + 1
	count = 1
	for i in start:officetotal
		push!(bottom, start)
		start += 1
	end
	return bottom
end

# ╔═╡ 85feb027-fe5a-4f5f-a1f6-44367dca05df
bottomfloor = generatebottomfloor(length(initialstate), num_of_offices)

# ╔═╡ 4680c044-e4ab-4ae8-bc6e-d22e63d6e698
function generatetopfloor(y)
 top = []
	for i in 1:y
		push!(top, i)
	end
	return top
end

# ╔═╡ 5e175bc6-7001-414e-9d15-3e6f7b5a4bb5
function perform_actions(current_state, storeys, offices)
	total_offices = lenght(current_state)
	right_wall = generaterightwall(total_offices, offices)
	left_wall = generateleftwall(total_offices, offices)
	bottom_floor = generatebottomfloor(total_offices, offices)
	top_floor = generatetopfloor(offices)

	possible_states = []

	current_office = get_current_office(current_state)
	if current_office.parcels > 0
		push!(possible_states, collect(state))
	end
	if current_office.position ∉ right_wall
		push!(possible_states, move_right(state))
	end
	if current_office.position ∉ left_wall
		push!(possible_states, move_left(state))
	end
	if current_office.position ∉ bottom_floor
		push!(possible_states, move_down(state))
	end
	if current_office.position ∉ top_floor
		push!(possible_states, move_up(state))
	end

	return decide_next_state(possible_states, storeys, offices)
end

# ╔═╡ 5786f6aa-6304-4552-bbcf-c2e7738f7b84
function search_algorithm(intial_state, storeys, offices)
	actions = []
	current_state = initial_state
	goal_states = create_goal_states(storeys, offices)
	
	while current_state ∉ goal_states
		current_state = perform_actions(current_state, storeys, office)
	end
	return actions
end

# ╔═╡ 90a14beb-7bc5-402f-a3b1-deb4c18ee3d7
topfloor = generatetopfloor(num_of_offices)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═e43dfac9-4415-47da-9efa-9bf41d23fa46
# ╟─9199ffd8-0b3f-45b3-8392-8480e05e312e
# ╟─16d2cef0-1dd1-40bd-9970-840e7cc47def
# ╠═bbfa0e76-f88a-4c7b-a657-e647ea08454c
# ╟─76289f98-8744-4604-bab0-97667db69ee8
# ╟─ff251553-055b-45a9-8c51-1097193fd78c
# ╟─f43d9737-d84c-4e6b-a6e8-c28508f19906
# ╟─31d07c38-67cc-485a-89d0-ed5245b68c6e
# ╟─0c2c0c8b-e3e6-4de5-9ed6-4b0a23866e60
# ╟─53208084-f453-41b6-8454-c44d59eee2fb
# ╟─bd840ecd-dcc4-4266-9b86-035a1aba7f7f
# ╟─3ca2a099-cd62-4dcb-bf0c-9f5024be67d1
# ╟─4663f817-2c70-4809-b121-3fae45c0e690
# ╠═5fa4eea8-1d3a-4840-aed3-d69d3a7ff7c1
# ╟─5cdbaf54-db81-4e65-99b9-602fbc854004
# ╠═09aab4c7-d334-445b-8c22-89cd0bafbadd
# ╠═4d7466d0-be0e-4be0-86a4-c497d9c3efca
# ╟─30757e4f-3409-46a7-916c-affd7381a4d7
# ╟─3b5eff55-91bc-4e2a-b3f6-e3712639e3ad
# ╟─4246d386-c09a-43db-8136-f41ae814c9d1
# ╟─466944b8-4292-4d2b-b28a-c33e43fb2661
# ╟─25f4bbb6-1421-48c9-873b-6e66d8ea7ac2
# ╠═d70d8486-3416-4ea2-bb5a-ab07a4052e7c
# ╟─3db063d0-bc59-45eb-85e3-34f21bbfb47d
# ╟─9bac7bc0-d697-4533-a7ff-1a46ca3b0ccf
# ╠═16ff7843-63c0-42aa-8344-f1695738a287
# ╟─be180abd-a629-4097-bd0c-cae9fd510ee8
# ╠═db322b58-f00b-40fd-9f79-0264e080229d
# ╟─93355dd6-56f5-4012-b9d4-54880c969d78
# ╟─ffee1e39-0b58-4b72-9488-bbb87227724a
# ╟─9cd8df79-bf0c-4be9-a9f5-699155f7aa59
# ╠═5a8d8d44-2104-4eb9-bb87-e7a42d27062f
# ╠═a3bc99e6-4330-41bf-b679-2f7562fd1108
# ╠═2887c30d-f356-4c61-bf93-b4a8bb7a1533
# ╠═5457f1ba-4d18-4765-9925-7b089a77be15
# ╠═39b56787-03dd-4ade-9fd1-40c82b76a40f
# ╟─3de71206-fc15-4cd7-bd01-f9d260633182
# ╠═5786f6aa-6304-4552-bbcf-c2e7738f7b84
# ╠═5e175bc6-7001-414e-9d15-3e6f7b5a4bb5
# ╠═34792a3d-c56c-4aeb-8d76-3dbcf26b87b6
# ╠═a46f5b8e-407c-47f7-8c6e-f2742307ee82
# ╟─dfed749d-1c7c-4f28-8a31-071abde42188
# ╠═a1724f33-54d0-4b80-b50f-56370cb76132
# ╠═fecacb61-edce-4bf5-a517-e5281a7f567a
# ╠═82a85a4c-776e-41cd-a64f-51cf96108e15
# ╟─6b673a17-8d43-4cef-a102-8b9ed15e538e
# ╠═7980c36d-ffd7-4c17-986a-ba9d3e706fe5
# ╠═40abb265-2f2e-4799-a5dc-a7dfc3008b0e
# ╠═85feb027-fe5a-4f5f-a1f6-44367dca05df
# ╠═1835eff7-6916-45e5-a5be-cebce87f6cf2
# ╠═90a14beb-7bc5-402f-a3b1-deb4c18ee3d7
# ╠═4680c044-e4ab-4ae8-bc6e-d22e63d6e698
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
